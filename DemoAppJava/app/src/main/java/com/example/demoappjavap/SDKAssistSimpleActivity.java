package com.example.demoappjavap;

import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssist;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistSimple;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistSimpleScrapResult;
import com.ksp.mobilesdkjava.MarketSystem.BOE.MarketOrderList;
import com.ksp.mobilesdkjava.MarketSystem.BOE.MarketStateBOE;
import com.ksp.mobilesdkjava.MarketSystem.BOE.e_MarketState;
import com.ksp.mobilesdkjava.MarketSystem.Base.e_MarketKeyCode;
import com.ksp.mobilesdkjava.OUtilSystem.OStringUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SDKAssistSimpleActivity extends AppCompatActivity {
    String GetEditTextValue(@IdRes int id) {
        EditText edit = (EditText) findViewById(id);

        return edit.getText().toString();
    }

    private void MessageBox(View view, String title, String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());

                alertDialogBuilder.setTitle(title);
                alertDialogBuilder.setMessage(message);

                alertDialogBuilder.create().show();

            }
        });
    }

    String root_path;
    String appName;
    SDKAssistSimple sdkInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdk_assist_simple);

        File root_file = this.getFilesDir();

        root_path = root_file.getAbsolutePath();
        appName = "sdkassist";

        try {
            sdkInstance = new SDKAssistSimple(root_path, appName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void OnGetAvailableMartClick(View view) {
        try {
            ArrayList<e_MarketKeyCode> martList = sdkInstance.GetAvailableMart();

            StringBuilder sb = new StringBuilder();
            for (e_MarketKeyCode mktCode : martList) {
                sb.append(mktCode);
                sb.append(OStringUtil.NewLine);
            }

            MessageBox(view, "사용가능 마트", sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            MessageBox(view, "실패", e.getMessage());
        }
    }

    public void OnGetMartState(View view) {
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run() {

                try {
                    MarketStateBOE stateBOE = sdkInstance.GetMarketState();

                    ArrayList<e_MarketKeyCode> avaMart = SDKAssist.GetAvailableMart();

                    StringBuilder sb = new StringBuilder();
                    for (e_MarketKeyCode mkt : avaMart) {
                        MarketStateBOE.State state = stateBOE.GetState(mkt);
                        if (state.StateCode == e_MarketState.Normal) {
                            sb.append(mkt.toString() + "정상");

                        } else {
                            sb.append(mkt.toString() + "상태:" + state.StateCode + " message:" + state.Message1 + " ," + state.Message2);
                            Log.d("mart state", state.MessageForDelveroper);
                        }
                        sb.append(OStringUtil.NewLine);
                    }

                    MessageBox(view, "마트상태", sb.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void OnScrapEmartClick(View view) throws Exception {
        new Thread(new Runnable() {
            public void run() {

                try {
                    String martId = "khw2000love";
                    String martPw = "jkhw0406";
                    e_MarketKeyCode mktKeyCode = e_MarketKeyCode.EMartApp;

                    SDKAssistSimpleScrapResult ret = sdkInstance.ScrapReceipt(mktKeyCode, martId, martPw, null);

                    if (ret.Success) {
                        MessageBox(view, "수집종료", "수집영수증:" + ret.ScrappedReceiptList.size() + OStringUtil.NewLine + "수집영수증상품:" + ret.ScrappedReceiptProductList.size());
                    } else {
                        MessageBox(view, "실패", ret.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();

    }

    public void OnScrapLotteClick(View view) {
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run() {

                try {
                    String martId = "2000love";
                    String martPw = "jkhw0406";
                    e_MarketKeyCode mktKeyCode = e_MarketKeyCode.LotteMCoupon;

                    SDKAssistSimpleScrapResult ret = sdkInstance.ScrapReceipt(mktKeyCode, martId, martPw, null);

                    if (ret.Success) {
                        MessageBox(view, "수집종료", "수집영수증:" + ret.ScrappedReceiptList.size() + OStringUtil.NewLine + "수집영수증상품:" + ret.ScrappedReceiptProductList.size());
                    } else {
                        MessageBox(view, "실패", ret.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }

    public void OnScrapHomePlusClick(View view) {
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run() {

                try {
                    String martId = "010-9192-6882";
                    String martPw = "박정식";
                    e_MarketKeyCode mktKeyCode = e_MarketKeyCode.HomePlus;

                    SDKAssistSimpleScrapResult ret = sdkInstance.ScrapReceipt(mktKeyCode, martId, martPw, null);

                    if (ret.Success) {
                        MessageBox(view, "수집종료", "수집영수증:" + ret.ScrappedReceiptList.size() + OStringUtil.NewLine + "수집영수증상품:" + ret.ScrappedReceiptProductList.size());
                    } else {
                        MessageBox(view, "실패", ret.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }

    public void OnRequestHomePlusAuthCodeClick(View view) {
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run() {

                try {
                    String userHP = "010-9192-6882";
                    String userName = "박정식";
                    String userBirth = "19740530";
               //     sdkInstance.RequestHomePlusAuthCode(userHP, userBirth, userName);

                    MessageBox(view, "홈플러스", "인증요청");
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "홈플러스 인증요청 실패", e.getMessage());
                }
            }
        }).start();
    }

    public void OnConfirmHomePlusAuthCodeClick(View view) {
        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run() {

                try {
                    String authCode = GetEditTextValue(R.id.idHomplusMobileCode);

                 //   sdkInstance.ConfirmHomePlusAuthCode(authCode);//LoginByMobileAuthCode

                    MessageBox(view, "홈플러스", "로그인완료");
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }
}
