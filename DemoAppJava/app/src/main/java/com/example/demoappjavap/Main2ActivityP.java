package com.example.demoappjavap;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssist;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistResult;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistScrapInnerResult;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistScrapResult;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistSvrType;
import com.ksp.mobilesdkjava.MarketSystem.BOE.MartBuyer;
import com.ksp.mobilesdkjava.MarketSystem.BOE.Receipt;
import com.ksp.mobilesdkjava.MarketSystem.BOE.ReceiptProduct;
import com.ksp.mobilesdkjava.MarketSystem.Base.e_MarketKeyCode;
import com.ksp.mobilesdkjava.OUtilSystem.OStringUtil;

import java.io.File;
import java.util.ArrayList;

public class Main2ActivityP extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void MessageBox(View view, String title, String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());

                alertDialogBuilder.setTitle(title);
                alertDialogBuilder.setMessage(message);

                alertDialogBuilder.create().show();

            }
        });
    }

    public void OnInitSDKClick(View view) {
        File root_file = this.getFilesDir();

        String sdk_path = root_file.getAbsolutePath();

        Context context = this;

        new Thread(new Runnable() {
            public void run() {

                try {
                    //SDKAssist.Init("DemoApp", "ReceiptTest_buyer1", "1e68091c-dd4b-4804-b6b0-62114c05226c");
                    SDKAssist.InitWithGADID("DemoApp", "9143C847-B81F-400D-B913-99E45D8ED162", sdk_path, context, SDKAssistSvrType.e_SvrType.AZURE);

                    MessageBox(view, "알림", "sdk초기화 성공");
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();

    }

    public void OnGetAvailableMartClick(View view) {

        try {
            ArrayList<e_MarketKeyCode> martList = SDKAssist.GetAvailableMart();

            StringBuilder sb = new StringBuilder();
            for (e_MarketKeyCode mktCode : martList) {
                sb.append(mktCode);
                sb.append(OStringUtil.NewLine);
            }

            MessageBox(view, "사용가능 마트", sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            MessageBox(view, "실패", e.getMessage());
        }

    }

    public void OnAddBuyerClick(View view) {
        new Thread(new Runnable() {
            public void run() {

                try {
                    SDKAssist.DeleteBuyer(e_MarketKeyCode.EMartApp, "khw2000love");
                    SDKAssistResult ret1 = SDKAssist.AddBuyer(e_MarketKeyCode.EMartApp, "khw2000love", "jkhw0406");

                    if (ret1.Success) {
                        MessageBox(view, "알림", "마트 아이디 추가 성공");
                    } else {
                        MessageBox(view, "실패", ret1.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();

    }

    public void OnGetMartBuyerListClick(View view) {

        try {
            ArrayList<MartBuyer> ret = SDKAssist.GetMartBuyerList();

            StringBuilder sb = new StringBuilder();
            for (MartBuyer mb : ret) {
                sb.append(mb.MktEnumKeyCode + "." + mb.MartId);
                sb.append(OStringUtil.NewLine);

            }


            MessageBox(view, "마트 아이디", sb.toString());

        } catch (Exception e) {
            e.printStackTrace();
            MessageBox(view, "실패", e.getMessage());
        }

    }

    public void OnScrapReceiptsClick(View view) {
        new Thread(new Runnable() {
            public void run() {

                try {
                    SDKAssistScrapResult ret = SDKAssist.ScrapReceipts();

                    if (ret.Success) {
                        if (ret.HasFailScrap()) {

                            StringBuilder sb = new StringBuilder();
                            sb.append(String.format("마트 영수증 수집 중 총%d중, %d건 수집 실패", ret.TryScarpCount, ret.ScrapFailList.size()));
                            for (SDKAssistScrapInnerResult innerResult : ret.ScrapFailList) {
                                Log.d("", String.format("마켓 %s, 아이디 $s, 수집 실패, 사유:%s", innerResult.MktEnumKeyCode, innerResult.MarketId, innerResult.Ex.getMessage()));
                            }
                            MessageBox(view, "알림", sb.toString());
                        } else {
                            MessageBox(view, "알림", "마트 영수증 수집 성공");
                        }

                    } else {
                        MessageBox(view, "실패1", ret.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패2", e.getMessage());
                }
            }
        }).start();
    }

    public void OnGetScrapedReceiptListClick(View view) {
        try {
            ArrayList<Receipt> receiptList = SDKAssist.GetScrapedReceiptList();

            StringBuilder sb = new StringBuilder();
            for (Receipt r : receiptList) {
                sb.append(r.MartEnumKeyCode.toString() + '.' + r.BuyDate + '.' + r.TotalAmount);
                sb.append(OStringUtil.NewLine);
                if (sb.length() > 100) {
                    break;
                }
            }


            MessageBox(view, "영수증리스트", sb.toString());

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void OnGetReceiptProductListByKeyClick(View view) {
        try {
            ArrayList<Receipt> receiptList = SDKAssist.GetScrapedReceiptList();
            if (receiptList.size() > 0) {
                Receipt rr = receiptList.get(1);

                ArrayList<ReceiptProduct> productArrayList = SDKAssist.GetReceiptProductListByKey(rr.ReceiptKey);

                StringBuilder sb = new StringBuilder();
                for (ReceiptProduct rp : productArrayList) {
                    sb.append(rp.Barcode + '.' + rp.UnitPrice + '.' + rp.BuyCount + '.' + rp.OrderAmount + '.' + rp.ProductName);
                    sb.append(OStringUtil.NewLine);
                    if (sb.length() > 100)
                        break;
                }
                MessageBox(view, "영수증 - " + rr.MartEnumKeyCode + '.' + rr.ReceiptKey, sb.toString());
            } else {
                MessageBox(view, "알림", "수집된 영수증이 없습니다.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            MessageBox(view, "실패", e.getMessage());
        }

    }

    public void OnHomePlusTest(View view) {
        Intent intent = new Intent(this, HomePlusLoginActivityP.class);

        startActivity(intent);
    }

    public void OnUpdateBuyerPasswordClick(View view) {
        new Thread(new Runnable() {
            public void run() {

                try {
                    SDKAssistResult ret1 = SDKAssist.UpdateBuyerPassword(e_MarketKeyCode.LotteMCoupon, "2000love", "jkhw0406");

                    if (ret1.Success) {
                        MessageBox(view, "알림", "마트 아이디 암호변경 성공");
                    } else {
                        MessageBox(view, "실패", ret1.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }
}
