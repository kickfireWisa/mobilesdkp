package com.example.demoappjavap;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssist;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistHomePlus;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistResult;
import com.ksp.mobilesdkjava.OUtilSystem.EH;
import com.ksp.mobilesdkjava.ui.UIUtil;

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class HomePlusLoginActivityP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_plus_login);
        UIUtil.SetTextOnTextView(this, R.id.idHomplusUserName, "박정식");
    }

    String GetEditTextValue(@IdRes int id) {
        EditText edit = (EditText) findViewById(id);

        return edit.getText().toString();
    }

    private void MessageBox(View view, String title, String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());

                alertDialogBuilder.setTitle(title);
                alertDialogBuilder.setMessage(message);

                alertDialogBuilder.create().show();

            }
        });
    }

    SDKAssistHomePlus mSdkHpCon = null;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void OnClickBtnRequestMobileAuthCode(View view) throws Exception {

        new Thread(new Runnable() {
            public void run() {

                try {
                    String hp = GetEditTextValue(R.id.idHomplusUserHP);
                    String name = GetEditTextValue(R.id.idHomplusUserName);
                    String birth = GetEditTextValue(R.id.idHomplusUserBirth);

                    mSdkHpCon = new SDKAssistHomePlus(hp, birth, name);

                    mSdkHpCon.RequestMobileAuthCode();

                    MessageBox(view, "인증코드 발송","인증코드를 입력하세요.");
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }

    public void OnClickBtnHomePlusLogin(View view) throws Exception {
        new Thread(new Runnable() {

            public void run() {

                try {
                    String authCode = GetEditTextValue(R.id.idHomplusMobileCode);
                    EH.ThrowIfFail(mSdkHpCon !=null, "먼저 모바일 인증코드를 요청해야 합니다.");
                    mSdkHpCon.LoginByMobileAuthCode(authCode);

                    SDKAssistResult ret =  mSdkHpCon.AddBuyerToSDKAssist();

                    if (ret.Success==false)
                    {
                        throw  ret.Ex;
                    }

                    MessageBox(view, "로그인완료","성공");
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();

    }

    public void OnClickBtnHomePlusLoginTest(View view) {
        new Thread(new Runnable() {

            public void run() {

                try {

                    String hp = GetEditTextValue(R.id.idHomplusUserHP);
                    String name = GetEditTextValue(R.id.idHomplusUserName);

                    SDKAssistHomePlus con = SDKAssistHomePlus.LoginTest(hp, name);

//                    SDKAssistResult ret = SDKAssist.AddBuyerHomePlus(con);
//                    if (ret.Success==false)
//                    {
//                        throw  ret.Ex;
//                    }

                    MessageBox(view, "로그인 테스트 완료","성공");
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }


}
