package com.example.demoappjavap;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssist;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistManager;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistResult;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistScrapInnerResult;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistScrapResult;
import com.ksp.mobilesdkjava.MSDKSystem.SDKAssistSystem.SDKAssistSvrType;
import com.ksp.mobilesdkjava.MarketSystem.BOE.MarketStateBOE;
import com.ksp.mobilesdkjava.MarketSystem.BOE.MartBuyer;
import com.ksp.mobilesdkjava.MarketSystem.BOE.Receipt;
import com.ksp.mobilesdkjava.MarketSystem.BOE.ReceiptProduct;
import com.ksp.mobilesdkjava.MarketSystem.BOE.e_MarketState;
import com.ksp.mobilesdkjava.MarketSystem.Base.e_MarketKeyCode;
import com.ksp.mobilesdkjava.OUtilSystem.OStringUtil;

import java.io.File;
import java.util.ArrayList;

public class Main3ActivityP extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3_p);
    }

    SDKAssistManager mgr = null;
    public void OnInitSDKClick(View view) {
        File root_file = this.getFilesDir();

        String sdk_path = root_file.getAbsolutePath();

        Context context = this;

        new Thread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run() {

                try {
                    mgr = new SDKAssistManager();
                    mgr.Init("DemoApp", "9143C847-B81F-400D-B913-99E45D8ED162", sdk_path, context, SDKAssistSvrType.e_SvrType.AZURE );

                    MessageBox(view, "알림", "sdk초기화 성공");
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();


    }
    private void MessageBox(View view, String title, String message) {
        runOnUiThread(new Runnable() { public void run() {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());

            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);

            alertDialogBuilder.create().show();

        } });
    }
    public void OnGetAvailableMartClick(View view) {
        try {
            ArrayList<e_MarketKeyCode> martList = mgr.GetAvailableMart();

            StringBuilder sb = new StringBuilder();
            for (e_MarketKeyCode mktCode : martList)
            {
                sb.append(mktCode);
                sb.append(OStringUtil.NewLine);
            }

            MessageBox(view, "사용가능 마트", sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            MessageBox(view, "실패", e.getMessage());
        }
    }

    public void OnAddBuyerClick(View view) {
        new Thread(new Runnable() {
            public void run() {

                try {
                    //mgr.DeleteBuyer(e_MarketKeyCode.EMartApp, "khw2000love");
                    //SDKAssistResult ret1 = mgr.AddBuyer(e_MarketKeyCode.EMartApp, "khw2000love", "jkhw0406", true);
                    SDKAssistResult ret1 = mgr.AddBuyer(e_MarketKeyCode.LotteMCoupon, "2000love", "jkhw0406", true);

                    if (ret1.Success)
                    {
                        MessageBox(view, "알림", "마트 아이디 추가 성공");
                    }
                    else {
                        MessageBox(view, "실패", ret1.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }

    public void OnGetMartBuyerListClick(View view) {
        try {
            ArrayList<MartBuyer> ret = mgr.GetMartBuyerList();

            StringBuilder sb = new StringBuilder();
            for (MartBuyer mb : ret)
            {
                sb.append(mb.MktEnumKeyCode+"."+mb.MartId);
                sb.append(OStringUtil.NewLine);

            }


            MessageBox(view, "마트 아이디", sb.toString());

        } catch (Exception e) {
            e.printStackTrace();
            MessageBox(view, "실패", e.getMessage());
        }

    }

    public void OnScrapReceiptsClick(View view) {
        new Thread(new Runnable() {
            public void run() {

                try {
                    SDKAssistScrapResult ret = mgr.ScrapReceipts();

                    if (ret.Success)
                    {
                        if (ret.HasFailScrap())
                        {

                            StringBuilder sb= new StringBuilder();
                            sb.append(String.format("마트 영수증 수집 중 총%d중, %d건 수집 실패", ret.TryScarpCount, ret.ScrapFailList.size()));
                            for (SDKAssistScrapInnerResult innerResult : ret.ScrapFailList)
                            {
                                Log.d("", String.format("마켓 %s, 아이디 %s, 수집 실패, 사유:%s",  innerResult.MktEnumKeyCode, innerResult.MarketId, innerResult.Ex.getMessage()));
                            }
                            MessageBox(view, "알림", sb.toString());
                        }
                        else
                        {
                            MessageBox(view, "알림", "마트 영수증 수집 성공");
                        }

                    }
                    else {
                        MessageBox(view, "실패1", ret.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패2", e.getMessage());
                }
            }
        }).start();
    }

    public void OnGetScrapedReceiptListClick(View view) {
        try {
            ArrayList<Receipt> receiptList = mgr.GetScrapedReceiptList();

            StringBuilder sb = new StringBuilder();
            for (Receipt r : receiptList)
            {
                sb.append(r.MartEnumKeyCode.toString()+'.'+r.BuyDate+'.'+r.TotalAmount);
                sb.append(OStringUtil.NewLine);
                if (sb.length()>100)
                {
                    break;
                }
            }

            MessageBox(view, "영수증리스트", sb.toString());

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void OnGetReceiptProductListByKeyClick(View view) {
        try {
            ArrayList<Receipt> receiptList = mgr.GetScrapedReceiptList();
            if (receiptList.size()>0)
            {
                Receipt rr = receiptList.get(1);

                ArrayList<ReceiptProduct> productArrayList = mgr.GetReceiptProductListByKey(rr.ReceiptKey);

                StringBuilder sb = new StringBuilder();
                for (ReceiptProduct rp : productArrayList)
                {
                    sb.append(rp.Barcode+'.'+rp.UnitPrice+'.'+rp.BuyCount+'.'+rp.OrderAmount+'.'+rp.ProductName);
                    sb.append(OStringUtil.NewLine);
                    if (sb.length()>100)
                        break;
                }
                MessageBox(view, "영수증 - "+rr.MartEnumKeyCode+'.'+rr.ReceiptKey, sb.toString());
            }
            else
            {
                MessageBox(view, "알림", "수집된 영수증이 없습니다.");
            }

        }catch (Exception e)
        {
            e.printStackTrace();
            MessageBox(view, "실패", e.getMessage());
        }
    }

    public void OnHomePlusTest(View view) {
        Intent intent = new Intent(this, HomePlusLoginActivityP.class);

        startActivity(intent);
    }

    public void OnGetMartStateClick(View view) {
        new Thread(new Runnable() {
            public void run() {

                try {
                    MarketStateBOE stateBOE = mgr.GetMarketState();

                    ArrayList<e_MarketKeyCode> avaMart = mgr.GetAvailableMart();

                    StringBuilder sb = new StringBuilder();
                    for (e_MarketKeyCode mkt : avaMart) {
                        MarketStateBOE.State state = stateBOE.GetState(mkt);
                        if (state.StateCode == e_MarketState.Normal) {
                            sb.append(mkt.toString() + "정상");

                        } else {
                            sb.append(mkt.toString() + "상태:" + state.StateCode + " message:" + state.Message1 + " ," + state.Message2);
                            Log.d("mart state", state.MessageForDelveroper);
                        }
                        sb.append(OStringUtil.NewLine);
                    }

                    MessageBox(view, "마트상태", sb.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }

    public void OnUpdateBuyerPasswordClick(View view) {
        new Thread(new Runnable() {
            public void run() {

                try {
                    SDKAssistResult ret1 = mgr.UpdateBuyerPassword(e_MarketKeyCode.LotteMCoupon, "2000love", "jkhw0406");

                    if (ret1.Success) {
                        MessageBox(view, "알림", "마트 아이디 암호변경 성공");
                    } else {
                        MessageBox(view, "실패", ret1.Ex.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MessageBox(view, "실패", e.getMessage());
                }
            }
        }).start();
    }
}
